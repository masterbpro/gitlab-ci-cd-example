# Gitlab CI/CD Example
[![pipeline status](https://gitlab.com/masterbpro/gitlab-ci-cd-example/badges/main/pipeline.svg)](https://gitlab.com/masterbpro/gitlab-ci-cd-example/-/commits/main)


#### Перед началом установки надо в настройках проекта `CI/CD -> Variables` установить перменные:
```env
DOCKER_USERNAME=username            # Ваша почта или имя пользователя в Docker Hub
DOCKER_TOKEN=AaBbCcDdEe1234567890   # Получаем из https://hub.docker.com/
```
![](docs/img/variables_settings.png)
___

## Настройка раннера
1. В настройках репозитория нужно перейти во вкладку `CI/CD -> Runners -> Expand` и взять оттуда регистрационный токен
![](docs/img/runner_settings.png)


2. Настройте репозитории для Debian/Ubuntu
```shell
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
```

3.  Установите раннер
```shell
apt install gitlab-runner
```

5. Запустите команду вместо `REGISTRATION_TOKEN` вставьте токен из первого шага
```shell
gitlab-runner register -n \
  --url https://gitlab.com/ \
  --registration-token REGISTRATION_TOKEN \
  --executor shell \
  --description "Gitlab master runner"
```

6. Добавьте пользователя `gitlab-runner` в группу `docker`
```shell
usermod -aG docker gitlab-runner
```

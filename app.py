from fastapi import FastAPI
import socket

app = FastAPI()


@app.get("/")
async def root():
    return {"message": "Hello World from CI/CD", "host": socket.gethostname()}


if __name__ == "__main__":
    import uvicorn

    uvicorn.run("app:app", host="0.0.0.0", port=8000)
